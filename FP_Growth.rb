#!/usr/bin/env ruby

require 'rarff'
require_relative 'rarff-patch.rb'

require 'tree'

require_relative 'Item'
require_relative 'Rule'
require_relative 'utils'

$WILDCARD = 'RHOADS-WILD'  # Wild card for matching

# Tiny struct for header table
Header_Entry = Struct.new(:item, :nodes)
Header_Table = Struct.new(:hash, :entries)

class FP_Growth
    attr_reader :params, :rel, :rules, :item_sets

    def initialize(params)
        @params = params

        @rel = Rarff::Relation.new
        @rel.parse(File.open(params[:file], 'r').read)
    end

    def run
        attributes = Array.new(@rel.attributes.length)
        # make array of unique attributes just in case we don't have them defined
        (0...@rel.attributes.length).each do |i|
            attributes[i] = @rel.instances.map {|n| n.at(i)}.uniq!
        end

        items = [] # unique items
        attributes.each_with_index { |value, i| value.each{ |n| items << FP_Item.new(i, n, rel.attributes.at(i).name) } }

        @values_to_name = Hash.new

        items.keep_if do |item|
            item.frequency = coverage([item], @rel.instances)  # calculate freq
            item.bMinimal = item.frequency >= params[:min_coverage]
            @values_to_name[item.value] = item.attribute_name if item.bMinimal # make dict so we can easily map vals to dicts
            item.bMinimal
        end

        # Sort by freq
        items.sort_by! {|item| item.frequency}
        # p items

        @instances = instances_to_items(@rel.instances)


        header_table = Header_Table.new(Hash.new, [])
        item_freq_hash = Hash.new 0  # Item frequency hash

        items.each do |item|
            item_freq_hash[item.value] = item.frequency
            header_table.entries << Header_Entry.new(item.dup, [])  # populate header table
            header_table.hash[item.value] = header_table.entries[-1]
        end

        # sort attributes in descending order by frequency value
        @instances.each do |row|
            row.sort_by! { |x| -item_freq_hash[x.value]}
        end

        root = make_tree(header_table, nil, nil)        

        puts 'FP Growth initial tree' if params[:print]
        root.print_tree if params[:print]

        item_sets = []

        header_table.entries.each do |entry|

            considering = entry.item

            entry.nodes.each do |node|
                item_set = get_potential_rules(considering, node)
                if item_set.length > 0
                    if !item_sets[-1].nil? && item_set.length == item_sets[-1].length && (item_set & item_sets[-1] == item_set)  # skip any dups
                        next
                    else
                        item_sets << item_set
                    end
                end
                node.remove_from_parent!
            end
        end

        clean_item_sets(item_sets)
        @item_sets = item_sets
        @rules = gen_rules(item_sets)

    end

    def clean_item_sets(item_sets)
        item_sets.each {|item_set| item_set.each {|item| item.frequency = nil}}
    end

    # update parents
    def get_potential_rules(item, node)
        parents = get_parents(node)
        parent_payloads = parents.map {|x| x.content}
        
        to_consider = []

        (1..parent_payloads.length). each do |i|
            tried = i
            count = 0
            @instances.each do |row|
                count += 1 if valid_row?(row, parent_payloads + [item])
            end

            to_consider << parent_payloads[-tried] if count >= params[:min_coverage]
            parent_payloads[-tried] = $WILDCARD
        end
        
        to_consider.reverse! << item if to_consider.length > 0

        return to_consider
    end


    def gen_rules(item_sets)
        rules = []
        item_sets.each do |item_set|
            length = item_set.length
            rule = FP_Rule.new('_', item_set)
            rules << rule if rule.calc_accuracy(@instances) >= @params[:min_accuracy]

            (item_set.length - 1).downto(1) do |index|
                last_range = length - index
                
                rule = FP_Rule.new(item_set.first(index), item_set.last(last_range))
                if rule.calc_accuracy(@instances) >= @params[:min_accuracy] 
                    rules << rule  
                end

            end
        end

        rules
    end 

    # update table header with node pointers if it matches
    def update_table_header_pointers(header_table, node)
        entry = header_table.hash[node.content.value]
        if not entry.nil? and node.content.value == entry.item.value and node.content.attribute_name == entry.item.attribute_name
            entry.nodes << node
        end
    end

    def make_tree(header_table, root, cur_node)

        if root.nil?
            root = Tree::TreeNode.new('ROOT')

            index_row_freq = Hash.new 0
            check_index = 0
            @instances.each do |row|
                val = row.at(check_index).value
                index_row_freq[val] += 1 if header_table.hash.key? val
            end

            index_row_freq.each do |key, val|
                new_item = FP_Item.new(nil, key, @values_to_name[key])
                new_item.frequency = val
                new_node = Tree::TreeNode.new(new_item.to_s, new_item)
                update_table_header_pointers(header_table, new_node)
                root << new_node
            end

            make_tree(header_table, root, root)
        else
            cur_node.children.each do |child|
                parents = get_parents_payload(child)
                check_index = parents.length + 1
                return root if check_index >= @instances.at(0).length  # base case

                index_row_freq = Hash.new 0
                parents << child.content  # insert child to check against row

                @instances.each do |row|
                    val = row.at(check_index).value
                    if header_table.hash.key?(val) and valid_row?(row, parents)  # only care about high freq sets and valid rows
                        index_row_freq[val] += 1
                    end
                end

                index_row_freq.each do |key, val|
                    new_item = FP_Item.new(nil, key, @values_to_name[key])
                    new_item.frequency = val
                    new_node = Tree::TreeNode.new(new_item.to_s, new_item)
                    update_table_header_pointers(header_table, new_node)
                    child << new_node
                end

                make_tree(header_table, root, child)
            end

        end

        root
    end

    # as we move down the list making FP-growth we need to check if elements in row match
    # where we are in a tree
    def valid_row?(row, parents, wildcard=$WILDCARD)
        parents.each_with_index do |parent, i|
            if parent.class != String or parent != wildcard
                if parent.value != row.at(i).value
                    return false 
                end
            end
        end

        return true
    end
end
