#!/usr/bin/env ruby

require 'rarff'
require_relative 'rarff-patch.rb'

require_relative 'Apriori'
require_relative 'FP_Growth'


require_relative 'options' 
params = getParams()

rel = Rarff::Relation.new
rel.parse(File.open(params[:file], 'r').read)
params[:print] = true


puts 'Dataset:'
puts rel, ''

puts 'Current configuration: '
params.each do |k, v|
    puts "#{k} = #{v}"
end
puts ''

puts '', '*'*100
puts 'Apriori:', '*'*100, ''
apriori = Apriori.new(params)
apriori.run()
puts '', "The #{params[:num_report]} best rules"
print_N_best(params[:num_report], apriori.rules)

puts '', '*'*100
puts 'FP_Growth:', '*'*100, ''
fp = FP_Growth.new(params)
fp.run
puts '', "The #{params[:num_report]} best rules"
print_N_best(params[:num_report], fp.rules)
