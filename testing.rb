#!/usr/bin/env ruby

require 'test/unit'
require 'rarff'

require_relative 'rarff-patch.rb'
require_relative 'Item'
require_relative 'Rule'
require_relative 'utils'
require_relative 'Apriori'
require_relative 'FP_Growth'

#  We want to make sure the functions we share across the project are correct
class TestUtilFunctions < Test::Unit::TestCase

    def setup
        @rel = Rarff::Relation.new
        @rel.parse(File.open('arff/weather.nominal.arff', 'r').read)
    end
 
    def test_dup
        assert(!dup?([]), "no duplicate on empty array")
        assert(!dup?([Item.new(1, 4)]), "no duplicate on array of 1")
        assert(dup?([Item.new(0, 0), Item.new(0, 0)]), "duplicate")
        assert(!dup?([Item.new(1, 4), Item.new(3, 25)]), "no duplicate")
        assert(dup?([Item.new(1, 4), Item.new(3, 25), Item.new(1, 4)]), "duplicate")
    end

    def test_coverage
        inst = @rel.instances
        assert_equal coverage('_', inst), @rel.instances.length, 'test wild card coverage'
        assert_equal coverage([], inst), 0, 'test empty array coverage'
        assert_equal coverage([Item.new(0, 'sunny')], inst), 5, 'check coverage on known value'
        assert_equal coverage([Item.new(0, 'sunny'), Item.new(1, 'hot')], inst), 2, 'check coverage on known value'
        assert_equal coverage([Item.new(0, 'rainy'), Item.new(4, 'no')], inst), 2, 'check coverage on known value'
        assert_equal coverage([Item.new(1, 'hot'), Item.new(2, 'high'), Item.new(3, 'FALSE')], inst), 2, 'check coverage on known value'
        assert_equal coverage([Item.new(4, 'no'), Item.new(0, 'sunny')], inst), 3, 'check coverage on known value'
        assert_equal coverage([Item.new(1, 'hot'), Item.new(2, 'high'), Item.new(3, 'not real')], inst), 0, 'check coverage on known value'
    end

    def test_instances_to_items
        assert instances_to_items(@rel.instances).all? { |x| x.class == Array and x.all? { |y| y.class == Item } }, 'instances transformed to items'
    end

end
 
def _test_item_sets(item_sets)
    
    #  Check for duplicates
    item_sets.each do |item_set|
        duplicate_attr = Hash.new
        item_set.each do |item|
            assert !duplicate_attr.key?(item.attribute_name), 'duplicate attribute values in itemset'
            duplicate_attr[item.attribute_name] = nil
        end
    end

    #  test all item sets meet size specification
    item_sets.each do |item_set|
        assert item_set.length <= @params[:max_size_set], 'max itemset size followed'
    end
end

class TestApriori < Test::Unit::TestCase

    def setup
        @rel = Rarff::Relation.new
        @rel.parse(File.open('arff/weather.nominal.arff', 'r').read)

        @params = {}
        @params[:file] = "arff/weather.nominal.arff"
        @params[:min_coverage] = 2
        @params[:max_size_set] = @rel.attributes.length
        @params[:min_coverage] = 2
        @params[:min_accuracy] = 5

        @apriori = Apriori.new(@params)
    end

    def test_gen_item_sets
        item_sets = @apriori.gen_item_sets

        _test_item_sets(item_sets)

        item_sets.each do |item_set|
            assert coverage(item_set, @rel.instances) >= @params[:min_coverage], 'item set meets min coverage'
        end
    end

    def test_gen_rules
        item_sets = @apriori.gen_item_sets
        rules = @apriori.gen_rules item_sets
        assert_equal rules.length, rules.uniq {|rule| rule.combine}.length, 'all rules are unique'


        count = 0
        rules.each do |rule|
            count += 1 if rule.accuracy >= @params[:min_accuracy]
        end
        assert_equal rules.length, count, 'all rules meet min accuracy'
    end
end


class TestFP_Growth < Test::Unit::TestCase
    def setup
        @rel = Rarff::Relation.new
        @rel.parse(File.open('arff/weather.nominal.arff', 'r').read)

        @params = {}
        @params[:file] = "arff/weather.nominal.arff"
        @params[:min_coverage] = 2
        @params[:max_size_set] = @rel.attributes.length
        @params[:min_coverage] = 2
        @params[:min_accuracy] = 5

        @fp = FP_Growth.new(@params)
        @fp.run
    end

    def test_item_sets
        item_sets = @fp.item_sets
        _test_item_sets(item_sets)
    end

    def test_gen_rules
        rules = @fp.rules

        assert_equal rules.length, rules.uniq {|rule| rule.combine}.length, 'all rules are unique'

        count = 0
        rules.each do |rule|
            count += 1 if rule.accuracy >= @params[:min_accuracy]
        end
        assert_equal rules.length, count, 'all rules meet min accuracy'

    end

end