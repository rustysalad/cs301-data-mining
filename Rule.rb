require_relative 'utils'

class Rule
    attr_reader :antecedent, :consequent
    attr_accessor :accuracy

    def initialize(antecedent, consequent)
        @antecedent=antecedent
        @consequent=consequent
    end

    def convert_to_sentence(maybe_array)
        return maybe_array.join(' and ') if maybe_array.respond_to?(:join)
        return '_' if maybe_array.nil?
        maybe_array.to_s
    end

    def to_s
        consq = convert_to_sentence(@consequent)
        ante = convert_to_sentence(@antecedent)
        "#{ante} ==> #{consq}"
    end

    def combine
        if @antecedent == '_'
            return @consequent
        end
        (@antecedent + ['+'] + @consequent)
    end

    def calc_accuracy(instances)
        cov_ante = coverage(@antecedent, instances)
        @accuracy = (cov_ante + coverage(@consequent, instances)).fdiv(cov_ante)
        return @accuracy
    end
end

class FP_Rule < Rule

    def calc_accuracy(instances)
        cov_ante = fp_coverage(@antecedent, instances)
        if cov_ante == 0
            @accuracy = 0
            return 0
        else
            @accuracy = (cov_ante + fp_coverage(@consequent, instances)).fdiv(cov_ante)
            return @accuracy
        end
    end

end
