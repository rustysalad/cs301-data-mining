
# Return true if item_combo contains two values from the same category
def dup?(item_combo)
    item_combo.each_with_index do |prev, i|
        item_combo.each_with_index do |after, k|
            next if i == k 
            return true if prev.id == after.id
        end 
    end

    return false
end

def coverage(item_set, instances)
    sum = 0
    isCovered = true

    return item_set.length if item_set.length == 0

    if item_set.class == String # special case for when antecedent == "_"
        return instances.length
    else
        instances.each do |row|
            item_set.each do |item|
                if row.at(item.id) != item.value
                    isCovered = false
                    break
                end
            end

            sum += 1 if isCovered
            isCovered = true
        end
    end

    return sum
end

def fp_coverage(item_set, instances)
    sum = 0

    return item_set.length if item_set.length == 0

    if item_set.class == String # special case for when antecedent == "_"
        return instances.length
    else
        instances.each do |row|
            row.each do |item|
                item_set.each do |other_item|
                    if item.value == other_item.value
                        sum += 1
                        break
                    end
                end
            end
        end
    end

    return sum
end

def print_N_best(n, rules)
    count = 0
    rules.sort_by! {|rule| rule.accuracy}

    (rules.length - 1).downto(0) do |index|
        rule = rules[index]
        puts rule.to_s + ' accuracy: ' + rule.accuracy.to_s 
        count += 1
        break if count >= n
    end
end

# get parents payload of provided node in an array
def get_parents_payload(node)
    get_parents(node).map {|x| x.content}
end

# get node parents of provided node in an array
def get_parents(node)
    cur_node = node.parent
    parents  = []

    while(not cur_node.nil? and not cur_node.content.nil?) do
        parents.unshift(cur_node)
        cur_node = cur_node.parent
    end

    parents
end

# Convert items to our class Items
def instances_to_items(instances)
    new_instances = []
    instances.each do |row|
        new_row = []
        row.each_with_index do |value, i|
            item = Item.new(i, value)
            new_row << item
        end
        new_instances << new_row
    end

    new_instances
end