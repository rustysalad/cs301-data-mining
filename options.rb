require 'optparse'

def getParams()

    options = {}

    OptionParser.new do |opts|
        opts.banner = "Usage: ./run.rb [options]"

        opts.on("-f FILENAME", "Filename input") do |v|
            options[:file] = v
        end

        opts.on("-c INT", "Minimum coverage requirements") do |v|
            options[:min_coverage] = v.to_i
        end

        opts.on("-s INT", "Maximum size of item sets to consider") do |v|
            options[:max_size_set] = v.to_i
        end

        opts.on("-a INT", "Minimum accuracy requirements") do |v|
            options[:min_accuracy] = v.to_i
        end

        opts.on("-r INT", "Number of best rules to report, optional") do |v|
            options[:num_report] = v.to_i
        end

        opts.on("-h", "help") do 
            puts opts
            exit
        end

    end.parse!

    if options[:file].nil?
        puts "-f FILENAME required"
        # options[:file] = "./arff/weather.numeric.arff"
        exit
    end

    if options[:num_report].nil?
        options[:num_report] = 4
    end

    if options[:min_coverage].nil?
        options[:min_coverage] = 2
    end

    if options[:max_size_set].nil?
        options[:max_size_set] = 3
    end

    if options[:min_accuracy].nil?
        options[:min_accuracy] = 2
    end


    return options
end