class Item
    attr_reader :id, :value, :attribute_name
    attr_accessor :coverage

    def initialize(id, value, attr_name=nil)
        @id=id
        @value=value
        # We don't need to carry this around, but it makes printing nice so eh
        @attribute_name = attr_name
    end

    def to_s
        if @attribute_name.nil?
            @value.to_s
        else
            "#{@attribute_name}=#{@value}"
        end
    end

    def ==(other)
        self.id == other.id and self.value == other.value
    end
end

class FP_Item < Item
    attr_accessor :frequency, :bMinimal

    def ==(other)
        self.attribute_name == other.attribute_name and self.value == other.value
    end

    def to_s
        if @frequency.nil?
            super()
        else
            super() + " (#{@frequency})"
        end
    end
end