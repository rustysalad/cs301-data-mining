#! /bin/bash

gem query --installed --name-matches rarff 2>&1 >/dev/null || gem install --user-install rarff
gem query --installed --name-matches tree 2>&1 >/dev/null || gem install --user-install tree


ruby main.rb "$@"