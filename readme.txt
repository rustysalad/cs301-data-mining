My project was written in Ruby and can be run through the provided run.sh. You can access the command line argument help menu by doing “./run.sh -h”. It will output:

Usage: ./run.sh [options]
    -f FILENAME                      Filename input
    -c INT                           Minimum coverage requirements
    -s INT                           Maximum size of item sets to consider
    -a INT                           Minimum accuracy requirements
    -r INT                           Number of best rules to report, optional
    -h                               help

The only required argument is filename. The rest of the arguments will default to something sensible if not specified. For example to run the algorithms on the dataset weather.nominal.arff you would do ./run.sh -f "arff/weather.nominal.arff". 

I have also included other test cases in a file called testing.rb other than the ones below in this port. The tests can be after run.sh has been executed at least once on your machine and once you have done that you can actually run the tests by running “ruby testing.rb”.