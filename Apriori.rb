#!/usr/bin/env ruby

require 'rarff'
require_relative 'rarff-patch.rb'

require_relative 'Item'
require_relative 'Rule'
require_relative 'utils'


class Apriori
    attr_reader :params, :rel, :rules

    def initialize(params)
        @params = params

        @rel = Rarff::Relation.new
        @rel.parse(File.open(params[:file], 'r').read)
        @generated_item_sets = nil
    end

    def gen_item_sets
        attributes = Array.new(@rel.attributes.length)
        # make array of unique attributes just in case we don't have them defined
        (0...@rel.attributes.length).each do |i|
            # rel.attributes.at(i).name -> i  
            attributes[i] = @rel.instances.map {|n| n.at(i)}.uniq!
        end

        items = [] # unique items
        attributes.each_with_index { |value, index| value.each{ |n| items << Item.new(index, n, rel.attributes.at(index).name) } }

        item_combos = (1..@params[:max_size_set]).flat_map { |n| items.combination(n).to_a }

        item_sets = []
        item_combos.each do |item_combo|
            if (item_combo.length < 2 or not dup?(item_combo))
                item_set = item_combo
                item_sets << item_set
            end
        end

        puts "item sets: " if @params[:print]
        # remove items sets that don't meet coverage
        item_sets.keep_if do |x| 
            cov = coverage(x, @rel.instances)
            puts x.to_s + ": Coverage #{cov}" if @params[:print] if cov >= @params[:min_coverage]
            cov >= @params[:min_coverage]
        end
    end

    def gen_rules(item_sets)
        rules = []
        item_sets.each do |item_set|
            length = item_set.length
            rule = Rule.new('_', item_set)
            rules << rule if rule.calc_accuracy(@rel.instances) >= @params[:min_accuracy]

            (item_set.length - 1).downto(1) do |index|
                last_range = length - index
                
                rule = Rule.new(item_set.first(index), item_set.last(last_range))

                if rule.calc_accuracy(@rel.instances) >= @params[:min_accuracy] 
                    rules << rule
                else
                    break # Don't try multiple consequent versions of a rule that doesn't work
                end
            end
        end

        rules
    end 

    def run
        item_sets = gen_item_sets
        @rules = gen_rules item_sets  # calculate possible rules
    end
end
